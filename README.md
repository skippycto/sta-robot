# README #

This is an empty Robot Framework project that includes a sample test, example test structure, and Jenkinsfile suitable for use with a pre-built Docker image containing Robot Framework



### Installation and Use ###

* Create a new repository on Github
* Add the Jenkinsfile and test folders to the repository
* From Jenkins add a new pipeline
* Navigate to the new repo
* Jenkins will pull the tests and the Jenkinsfile then automatically build the pipeline and run it

